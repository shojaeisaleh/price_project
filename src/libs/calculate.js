// hour, price, experience, level
class Calculate {
    constructor (into, design, programming, city) {
        this.into = into
        this.design = design
        this.programming = programming
        this.city = city
    }
    multiply (accumulator, currentValue) {
        return accumulator * currentValue
    }

    calc_into (into = this.into) {
        let result = Object.values(into)
        return result.reduce(this.multiply);
    }

    calc_design (design = this.design) {
        let result = Object.values(design)
        return result.reduce(this.multiply);
    }

    calc_programming (programming = this.programming) {
        let result = Object.values(programming)
        return result.reduce(this.multiply);
    }
    
    get result () {
        let result = this.calc_into() + this.calc_design() + this.calc_programming()
        let days =
            Math.round((this.into.hour / 8)) +
            Math.round((this.design.hour / 8)) +
            Math.round((this.programming.hour / 8))
        return {
            into: this.calc_into(),
            design: this.calc_design(),
            programming: this.calc_programming(),
            result,
            days
        }
    }
}

export { Calculate }