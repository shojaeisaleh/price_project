import React, { Component } from 'react'
import StepWizard from 'react-step-wizard'
import {
  Navbar,
  NavbarBrand,
  Container,
  Card
} from 'reactstrap'

import { Step1, Step2, Step3, Final } from './pages'

export default class App extends Component {
  state = {
    step1: {
      hour: 1,
      price: 1,
      experience: 1,
      level: 1,
      city: 1
    },
    step2: {
      hour: 1,
      price: 1,
      experience: 1,
      level: 1,
      city: 1
    },
    step3: {
      hour: 1,
      price: 1,
      experience: 1,
      level: 1,
      city: 1
    }
  }

  handleChangeValue_step1 = (event) => {
    const target = event.target
    this.setState(prevState => ({
      step1: {
        ...prevState.step1,
        [target.name]: target.value
      }
    }))
  }

  handleChangeValue_step2 = (event) => {
    const target = event.target
    this.setState(prevState => ({
      step2: {
        ...prevState.step2,
        [target.name]: target.value
      }
    }))
  }

  handleChangeValue_step3 = (event) => {
    const target = event.target
    this.setState(prevState => ({
      step3: {
        ...prevState.step3,
        [target.name]: target.value
      }
    }))
  }

  render() {
    return (
      <React.Fragment>
        <Navbar
          color="dark"
          dir="rtl"
          dark
          expand="lg"
          className="shadow"
        >
          <NavbarBrand style={{ color: 'white' }}>
            قیمت پروژه
          </NavbarBrand>
        </Navbar>
        <Container className="mt-4">
          <Card className="bg-transparent text-right">
            <StepWizard>
              <Step1
                onChangeValue={this.handleChangeValue_step1}
              />
              <Step2
                onChangeValue={this.handleChangeValue_step2}
              />
              <Step3
                onChangeValue={this.handleChangeValue_step3}
              />
              <Final data={this.state} />
            </StepWizard>
          </Card>
        </Container>
      </React.Fragment>
    )
  }
}