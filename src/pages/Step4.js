import React from 'react'

import {
  CardTitle,
  Button,
  CardBody,
  CardFooter,
  ListGroup, ListGroupItem
} from 'reactstrap'

import { Calculate } from '../libs/calculate'

const Step4 = (props) => {
  const calc = new Calculate({
      hour: 10,
      price: 17000,
      experience: 1,
      level: 2,
      city: 1
  }, {
      hour: 20,
      price: 19000,
      experience: 1,
      level: 0.5,
      city: 1
  }, {
      hour: 30,
      price: 40000,
      experience: 2,
      level: 1.5,
      city: 1
  })

  console.log(calc.result)
  return (
    <CardBody>
      <CardTitle>نمایش نتیجه</CardTitle>
        <ListGroup>
          <ListGroupItem>Cras justo odio</ListGroupItem>
          <ListGroupItem>Dapibus ac facilisis in</ListGroupItem>
          <ListGroupItem>Morbi leo risus</ListGroupItem>
          <ListGroupItem>Porta ac consectetur ac</ListGroupItem>
          <ListGroupItem>Vestibulum at eros</ListGroupItem>
        </ListGroup>
      <CardFooter>
        <Button onClick={props.previousStep} size="sm" color="danger" className="float-left">
          پس‌تر
        </Button>
        <Button onClick={props.nextStep} size="sm" color="success" className="float-right">
          پیش‌تر
        </Button>
      </CardFooter>
    </CardBody>
  )
}

export { Step4 }
