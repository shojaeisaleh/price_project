import React, { Component } from 'react'
import {
    CardTitle,
    Button,
    CardBody,
    CardFooter,
    ListGroup,
    ListGroupItem
} from 'reactstrap'

import { Calculate } from '../libs/calculate'

const formatNumber = num => num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')

class Final extends Component {
    render() {
        const { data } = this.props
        const calc = new Calculate (data.step1, data.step2, data.step3)
        let result = calc.result
        console.log(result)
        return (
            <CardBody>
                <CardTitle>نمایش نتیجه</CardTitle>
                <ListGroup className="text-white">
                    <ListGroupItem className="bg-dark">
                        هزینه‌ی پیش‌تولید: {formatNumber(result.into)}
                    </ListGroupItem>
                    <ListGroupItem className="bg-dark">
                        هزینه‌ی طراحی رابط کاربری: {formatNumber(result.design)}
                    </ListGroupItem>
                    <ListGroupItem className="bg-dark">
                        هزینه‌ی برنامه نویسی: {formatNumber(result.programming)}
                    </ListGroupItem>
                    <ListGroupItem className="bg-dark">
                        هزینه‌ی کل: {formatNumber(result.result)}
                    </ListGroupItem>
                    <ListGroupItem className="bg-dark">
                        روزهای کاری تا پایان پروژه: {formatNumber(result.days)}
                    </ListGroupItem>
                </ListGroup>
                <CardFooter>
                    <Button onClick={this.props.previousStep} size="sm" color="danger" className="float-left">
                    پس‌تر
                    </Button>
                </CardFooter>
            </CardBody>
        )
    }
}

export { Final }