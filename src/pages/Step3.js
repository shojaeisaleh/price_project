import React from 'react'

import {
  CardTitle,
  CustomInput,
  Form,
  Button,
  FormGroup,
  Label,
  Input,
  CardBody,
  CardFooter
} from 'reactstrap'

const Step3 = (props) => {
  return (
    <CardBody>
      <CardTitle>برنامه نویسی پروژه</CardTitle>
      <Form>
        <FormGroup>
          <Label>ساعات مورد نیاز</Label>
          <Input
            type="number"
            min="1"
            max="100"
            name="hour"
            onChange={props.onChangeValue}
          />
        </FormGroup>
        <FormGroup>
          <Label>قیمت هر ساعت</Label>
          <Input
            type="select"
            name="price"
            onChange={props.onChangeValue}
          >
            <option value="40000">برنامه نویسی اندروید: ۴۰,۰۰۰</option>
            <option value="30000">فرانت وب: ۳۰,۰۰۰</option>
            <option value="50000">بک‌اند وب: ۵۰,۰۰۰</option>
            <option value="10000">ای‌پی‌آی: ۱۰,۰۰۰</option>
          </Input>
        </FormGroup>
        <FormGroup>
          <Label>مهارت شما</Label>
          <Input
            type="select"
            name="experience"
            onChange={props.onChangeValue}
          >
            <option value="0.5">مبتدی</option>
            <option value="1">متوسط</option>
            <option value="1.5">خوب</option>
            <option value="2">حرفه‌ای</option>
          </Input>
        </FormGroup>
        <FormGroup>
          <Label>سطح پروژه</Label>
          <CustomInput
            type="range"
            name="level"
            defaultValue={1.5}
            onChange={props.onChangeValue}
            min="1"
            max="2"
            step="0.1"
          />
        </FormGroup>
        <FormGroup>
          <Label>شهر / منطقه</Label>
          <Input
            name="city"
            onChange={props.onChangeValue}
            type="select"
          >
            <option value="2">تهران</option>
            <option value="1.75">مشهد، کرج، اصفهان، تبریز</option>
            <option value="1.5">شیراز، قم، ارومیه، رشت</option>
            <option value="1">سایر کلان‌شهرها</option>
            <option value="0.5">سایر</option>
          </Input>
        </FormGroup>
      </Form>
      <CardFooter>
        <Button onClick={props.previousStep} size="sm" color="danger" className="float-left">
          پس‌تر
        </Button>
        <Button onClick={props.nextStep} size="sm" color="success" className="float-right">
          پیش‌تر
        </Button>
      </CardFooter>
    </CardBody>
  )
}

export { Step3 }
